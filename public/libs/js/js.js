$(document).ready(function() {
	var menuBtn = $('.top-nav__btn');
	var menu = $('.top-nav__menu');
	var sidebarBtn = $('.left-sidebar__btn');
	var sidebarmenu = $('.left-sidebar__menu');

	menuBtn.on('click', function(event) {
		event.preventDefault();
		menu.toggleClass('top-nav__menu__active');
	});
	sidebarBtn.on('click', function(event) {
		event.preventDefault();
		sidebarmenu.toggleClass('left-sidebar__menu__active');
	});
	$('.directions-blocks').slick({
		arrows: false,
		dots: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slideToScroll: 1,
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slideToScroll: 1,
				}
			},
		]
	});

	var elem = document.querySelector('.calc-range');
	var init = new Powerange(elem, {min: 100000, max: 3000000, start: 100000, hideRange: true, step: 100000});
	
	var per, month, result, total, monthly;
	var money = +$('.calc-range').val();

	

	$('input[name="programs"]').on('change', function(event) {
		month = +$(this).attr('data-month');
	    per = +$(this).attr('data-per');

	    result = Math.round(per / 12 * month * money); //заработок
	    total = result + money;
	    monthly = parseInt(result / month);
	    $('.calc-monthly span').text(monthly.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
	    $('.calc-total span').text(total.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		$('.calc-total small span').text(month.toString());
	});

	$('.calc-range').on('change', function(event) {
		$('.calc-summ-invest__num span').text($(this).val().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
		var radio = $('input[name="programs"]:checked');
		money = +$(this).val();
		month = +radio.attr('data-month');
	    per = +radio.attr('data-per');
	    result = Math.round(per / 12 * month * money); //заработок
	    total = result + money;
	    monthly = parseInt(result / month);
	    $('.calc-monthly span').text(monthly.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
	    $('.calc-total span').text(total.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		$('.calc-total small span').text(month.toString());
	});
});